#include <sys/ioctl.h>
#include <fcntl.h>
#include <libsocketcan.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label_2->hide();

    initcan(0);
    initcan(1);
    startcan(0);
    startcan(1);
    currentcan = socketcan[0];
}

MainWindow::~MainWindow()
{
    delete ui;
    stopcan(0);
    stopcan(1);
}

void MainWindow::msg(QString str, int socket)
{
//    if(currentcan == socket)
    if(socketcan[0] == socket){
        if(ui->textEditcan0->toPlainText().count() > 100){
            ui->textEditcan0->clear();
        }
        ui->textEditcan0->append(QString2Hex(str));
    }
    else{
        if(ui->textEditcan1->toPlainText().count() > 100){
            ui->textEditcan1->clear();
        }
        ui->textEditcan1->append(QString2Hex(str));
    }
    qDebug () << "socket " << socket << "get msg:" << QString2Hex(str);
}

void MainWindow::closeEvent(QCloseEvent *)
{
    destroy();
    exit(0);
}

void MainWindow::initcan(int v)
{
    int ret = 0;

    if((can_set_bitrate(v == 0 ? "can0" : "can1", 500000)) ==0){
        can_do_start(v == 0 ? "can0" : "can1");
    }
    else
    {
        can_do_stop(v == 0 ? "can0" : "can1");
        can_set_bitrate(v == 0 ? "can0" : "can1", 500000);
        can_do_start(v == 0 ? "can0" : "can1");
    }

    socketcan[v] =  ::socket(PF_CAN,SOCK_RAW,CAN_RAW);

    struct ifreq ifr;
    strcpy((char *)(ifr.ifr_name),v == 0 ? "can0" : "can1");
    ioctl(socketcan[v],SIOCGIFINDEX,&ifr);

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    ret = bind(socketcan[v],(struct sockaddr*)&addr,sizeof(addr));
    if (ret < 0)
     {
     QMessageBox::about(this,"error","in bind error");
     exit(1);
     }

    t[v] = NULL;
    t[v] = new Thread(socketcan[v]);

    connect(t[v],SIGNAL(msg(QString, int)),this,SLOT(msg(QString, int)));

}

void MainWindow::startcan(int v)
{
    t[v]->start();
}

void MainWindow::stopcan(int v)
{
    if(t[v])
    {
        t[v]->stop();
        t[v]->deleteLater();
    }

    ::close(socketcan[v]);
}

void MainWindow::on_can0_toggled(bool checked)
{
    if(checked)
    {
        currentcan = socketcan[0];
    }
}

void MainWindow::on_can1_toggled(bool checked)
{
    if(checked)
    {
        currentcan = socketcan[1];
    }
}

char MainWindow::ConvertHexChar (char ch)
{
  if ((ch >= '0') && (ch <= '9'))
    return ch - 0x30;
  else if ((ch >= 'A') && (ch <= 'F'))
    return ch - 'A' + 10;
  else if ((ch >= 'a') && (ch <= 'f'))
    return ch - 'a' + 10;
  else
    return (-1);
}

QByteArray MainWindow::QString2Hex (QString str)
{
  QByteArray senddata;
  int hexdata, lowhexdata;
  int hexdatalen = 0;
  int len = str.length ();
  senddata.resize (len / 2);
  char lstr, hstr;
  for (int i = 0; i < len;)
    {
      hstr = str[i].toLatin1 ();
      if (hstr == ' ')
        {
          i++;
          continue;
        }
      i++;
      if (i >= len)
        break;
      lstr = str[i].toLatin1 ();
      hexdata = ConvertHexChar (hstr);
      lowhexdata = ConvertHexChar (lstr);
      if ((hexdata == 16) || (lowhexdata == 16))
        break;
      else
        hexdata = hexdata * 16 + lowhexdata;
      i++;
      senddata[hexdatalen] = (char) hexdata;
      hexdatalen++;
    }
  senddata.resize (hexdatalen);
  return senddata;
}
