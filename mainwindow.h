#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <linux/can.h>
#include "thread.h"
#include <QButtonGroup>
#define MAXCAN 2

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void closeEvent(QCloseEvent *);
private slots:
    void msg(QString str, int socket);
    void initcan(int v);
    void stopcan(int v);
    void startcan(int v);
    void on_can0_toggled(bool checked);
    void on_can1_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    int socketcan[MAXCAN];
    int currentcan;
    struct sockaddr_can addr;
    Thread *t[MAXCAN];
    QButtonGroup* btg;

    char ConvertHexChar (char ch);
    QByteArray QString2Hex (QString str);
};

#endif // MAINWINDOW_H
