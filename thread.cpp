#include "thread.h"

Thread::Thread(int s,QObject *parent) :
    QThread(parent)
{
    socket  = s;
    running = true;
}

void Thread::run()
{
    int nbytes;
    int len;
    struct can_frame frame;
    struct sockaddr_can addr;
    char buf[10];

    while(running)
    {
        nbytes=recvfrom(socket,&frame,sizeof(struct can_frame),0,(struct sockaddr *)&addr,(socklen_t*)&len);
        if(nbytes>0){
            memset(buf,0,sizeof(buf));
            strncpy(buf,(char*)frame.data,8);
            emit msg(QString(buf), socket);
        }
    }

}

void Thread::stop()
{
    running = false;
}
